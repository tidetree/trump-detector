package com.james;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RouteBuilder1 extends RouteBuilder {

    @Autowired
    TrumpService ts;

	@Override
	public void configure() throws Exception {

		from("timer:logMessageTimer?period=5s")
		.log("Event triggerd by ${property.CamelTimerName} at ${header.CamelTimerFiredTime}")
        .to("bean:trumpService?method=detectTrump")
		.to("log:?level=INFO&showBody=true");
	}

}
