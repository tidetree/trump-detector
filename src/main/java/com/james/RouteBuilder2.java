package com.james;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.stereotype.Component;

@Component
public class RouteBuilder2 extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		from("direct:upperCase")
		.log("Pre-uppercase: ${body}")
		.to("bean:stringUtils?method=upperCase")
		.to("log:?level=INFO&showBody=true&showHeaders=true")
		.log("Post-uppercase: ${body}");
		
		rest("/say")
        .get("/hello").to("direct:hello")
        .get("/bye").consumes("application/json").to("direct:bye")
        .post("/bye").to("mock:update");

    from("direct:hello")
        .transform().simple("$$$$ Hello World: ${headers.breadcrumbId} ++++");
    from("direct:bye")
        .transform().constant("Bye World");
    
    restConfiguration().component("netty4-http").bindingMode(RestBindingMode.json).port("8081")
    .contextPath("/trump/rest");
    
	}

}
