package com.james;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class AppTest {

  @Test
  public void testMultiply() {
        assertEquals("10 x 5 must be 50", 50, 50);
  }
}