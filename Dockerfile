FROM centos:7
EXPOSE 8080
RUN yum install -y java-1.8.0-openjdk
ADD target/trump-detector-1.0-SNAPSHOT.jar /
CMD ["java","-jar","/trump-detector-1.0-SNAPSHOT.jar"]
